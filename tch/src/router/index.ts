import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/home/Home.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import("../views/AboutView.vue"),
    // },
    {
      path: "/product-list",
      name: "ProductList",
      component: () => import("../views/order/ProductList.vue"),
    },

    {
      path: "/product/:slug",
      name: "ProductDetail",
      component: () => import("../views/order/ProductDetail.vue"),
    },

    {
      path: "/blogs",
      name: "NewsFeed",
      component: () => import("../views/blogs/News.vue"),
    },
    {
      path: "/blogs/:id",
      name: "PostNews",
      component: () => import("../views/blogs/Post.vue"),
    },

    {
      path: "/stores",
      name: "Store",
      component: () => import("../views/stores/Stores.vue"),
    },
    {
      path: "/promotion",
      name: "Promotion",
      component: () => import("../views/promotion/Promotion.vue"),
    },
  ],
});

export default router;
