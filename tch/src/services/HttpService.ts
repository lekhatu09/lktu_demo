import axios from "axios";

export const HttpService = {
  tch: axios.create({
    baseURL: import.meta.env.VITE_TCH_API_URL as string,
    headers: {
      src: "TCH-WEB",
      "TCH-APP-VERSION": "5.3.0",
      "TCH-DEVICE-ID": "272efdb7-2d7e-4e5c-82b2-95d258739a2e",
    },
  }),
};

//console.log(import.meta.env.VITE_TCH_API_URL);
