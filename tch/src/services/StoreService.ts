import type { Stores } from "@/models/stores/Stores";
import { HttpService } from "./HttpService";

export const StoreService = {
  getStore: async () =>
    await HttpService.tch.get("stores/all").then(({ data }) => {
      //console.log(data);
      return data.store as Stores[];
    }),
};
