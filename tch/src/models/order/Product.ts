import type { Options } from "./Options";

export interface Product {
  base_price: number;
  base_price_str: number;
  description: string;
  description_html: null;
  hint_note: string[];
  id: string;
  image: string[];
  is_delivery: boolean;
  is_pickup: boolean;
  name: string;
  options: Options[];
  price: number;
  price_str: string;
  promotion: null;
  recommended_id: null;
  s_base_price: null;
  s_price: null;
  slug: string;
  status: number;
  thumbnail: string;
}
