export interface Items {
  code: number;
  id: number;
  is_main: boolean;
  localize: {
    en: string;
    vi: string;
  };
  name: string;
  price: number;
  price_str: string;
  product_id: number;
  val: string;
}
